﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof(Text))]
public class StarDisplay : MonoBehaviour {
	
	private Text starText;
	private int stars = 10; //initial quantity of stars for each level
	public enum Status {SUCCESS, FAILURE};

	// Use this for initialization
	void Start () 
	{
		starText = GetComponent<Text>();
		UpdateDisplay ();
	}		
	public void AddStars() //called in star's animation
	{
		stars += Random.Range(2,4);
		UpdateDisplay();
	}
	public Status UseStars(int amount)
	{
		if(stars >=amount)
		{			
			stars-=amount;
			UpdateDisplay();
			return Status.SUCCESS;
		}
		else
		{
			return Status.FAILURE;
		}
	}
	private void UpdateDisplay()
	{
		starText.text = stars.ToString();
	}
}
