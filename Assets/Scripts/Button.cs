﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Button : MonoBehaviour {

	public GameObject defenderPrefab;
	
	private Button[] buttonArray;
	public static GameObject selectedDefender;
	private LevelManager levelManager;
	private Text costText;
	private DefenderSpawner defenderSpawner;
	private GameTimer gametimer;
	
	
	// Use this for initialization
	void Start () {
		defenderSpawner = GameObject.FindObjectOfType<DefenderSpawner>();
		buttonArray = GameObject.FindObjectsOfType<Button>();	
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		gametimer = GameObject.FindObjectOfType<GameTimer>();
		costText = GetComponentInChildren<Text>();
		if(!costText)
		{
			Debug.LogWarning(name+" has no cost text");
		}
		costText.text = defenderPrefab.GetComponent<Defender>().starCost.ToString();
		
		foreach(Button thisButton in buttonArray)
		{
			if(thisButton.name == "Sun Trophy") //only allow trophy at start
			{							
				thisButton.GetComponent<SpriteRenderer>().color = Color.grey;				
			}
			else
			{
				thisButton.GetComponent<SpriteRenderer>().color = Color.black;
			}
		}				
	}
	
	public void SetDefenderAvailabilityForLevel()
	{
		foreach(Button thisButton in buttonArray)
		{
			if(levelManager.GetAvailableDefenders().Contains(thisButton.name))
			{
				
				if(thisButton == GetComponent<Button>())
				{
					GetComponent<SpriteRenderer>().color = Color.white;					
					selectedDefender = defenderPrefab;
				}
				else
				{				
					thisButton.GetComponent<SpriteRenderer>().color = Color.grey;
				}		
			}
			else
			{
				thisButton.GetComponent<SpriteRenderer>().color = Color.black;
			}
		}
	}
		
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnMouseDown()
	{			
		selectedDefender = null;
		foreach(Button thisButton in buttonArray)
		{		
			if(levelManager.GetAvailableDefenders().Contains(thisButton.name))
			{
				if(gametimer.GetFirstMoveMade() == true || thisButton.name == "Sun Trophy")
				{
					if(thisButton == GetComponent<Button>())
					{
						GetComponent<SpriteRenderer>().color = Color.white;
						selectedDefender = defenderPrefab;
						defenderSpawner.selectedButton = this; // done so the spawner initially knows how to set button highlights
					}
					else
					{				
						thisButton.GetComponent<SpriteRenderer>().color = Color.grey;
					}
				}
			}
			else
			{
				thisButton.GetComponent<SpriteRenderer>().color = Color.black;
			}
		}
	}
}
