﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Rigidbody2D))]
public class Attacker : MonoBehaviour {
	
	[Tooltip ("Average number of seconds between appearances")]
	public float currentSpeed, seenEverySeconds;
	private GameObject currentTarget;	
	private Animator anim;
	
	void Start () {
		Rigidbody2D myRigidbody = gameObject.GetComponent<Rigidbody2D>();
		myRigidbody.isKinematic = true;		
		anim = GetComponent<Animator>();		
	}
	
	void Update () {
		transform.Translate (Vector3.left * currentSpeed * Time.deltaTime);
		if(!currentTarget)
		{
			anim.SetBool("isAttacking", false);
		}		
	}
	
	void OnTriggerEnter2D()
	{
		//Debug.Log (name + " trigger enter ");
	}
	
	public void SetSpeed(float speed)
	{
		currentSpeed = speed;
	}
	
	//Called from animator at the time of the actual attack
	public void StrikeCurrentTarget(float damage)
	{
		if(currentTarget)
		{
			Health health = currentTarget.GetComponent<Health>();
			if(health)
			{
				health.DealDamage(damage);
			}
		}		
	}
	public void Attack(GameObject obj)
	{
		currentTarget=obj;
		
	}
}