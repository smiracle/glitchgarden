﻿using UnityEngine;
using System.Collections;

public class DefenderSpawner : MonoBehaviour {

	public Camera myCamera;
		
	private GameObject parent;
	private StarDisplay starDisplay;
	private GameTimer gametimer;
	public Button selectedButton;
	
	void Start()
	{
		parent = GameObject.Find ("Defenders");
		starDisplay = GameObject.FindObjectOfType<StarDisplay>();
		gametimer = GameObject.FindObjectOfType<GameTimer>();
		
		if(!parent)
		{
			parent = new GameObject("Defenders");			
		}
	}	
	
	void OnMouseDown()
	{
		Vector2 rawPos = CalculateWorldPointOfMouseClick();
		Vector2 roundedPos = SnapToGrid(rawPos);
		GameObject defender = Button.selectedDefender;
		
		if(defender != null)
		{		
			if(gametimer.GetFirstMoveMade() == false)
			{
				gametimer.SetFirstMoveMade(true);	
				if(selectedButton != null)
				{			
					selectedButton.SetDefenderAvailabilityForLevel();
				}
				else
				{
					Debug.LogWarning ("selectedButton in DefenderSpawner.cs null");
				}
			}
		
			int defenderCost = defender.GetComponent<Defender>().starCost;
			if(starDisplay.UseStars(defenderCost) == StarDisplay.Status.SUCCESS)
			{
				SpawnDefender (roundedPos, defender);
			}
			else
			{
				Debug.Log ("insufficient stars available to spawn");
			}
		}
	}	

	void SpawnDefender (Vector2 roundedPos, GameObject defender)
	{
		Quaternion zeroRot = Quaternion.identity;
		GameObject newDef = Instantiate (defender, roundedPos, zeroRot) as GameObject;
		newDef.transform.parent = parent.transform;
	}

	Vector2 SnapToGrid(Vector2 rawWorldPos)
	{		
		return new Vector2(Mathf.RoundToInt(rawWorldPos.x), Mathf.RoundToInt(rawWorldPos.y));
	}	
	Vector2 CalculateWorldPointOfMouseClick()
	{
		float mouseX = Input.mousePosition.x;
		float mouseY = Input.mousePosition.y;
		float distanceFromCamera = 10f;
		
		Vector3 weirdTriplet = new Vector3(mouseX,mouseY,distanceFromCamera);
		Vector2 worldPos = myCamera.ScreenToWorldPoint(weirdTriplet);
		return worldPos;
	}
}
