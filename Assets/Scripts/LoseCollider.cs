﻿using UnityEngine;
using System.Collections;

public class LoseCollider : MonoBehaviour {

	private LevelManager levelManager;
	
	// Use this for initialization
	void Start () {
		levelManager = GameObject.FindObjectOfType<LevelManager>();		
	}	
	
	void OnTriggerEnter2D()
	{
		Debug.Log ("lose collider entered!");
		levelManager.LoadLevel("03b Lose");
	}
}
