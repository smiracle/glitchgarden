﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Attacker))]

public class Fox : MonoBehaviour {
	
	private Animator anim;
	private Attacker attacker;
	
	void Start () {
		anim = GetComponent<Animator>();
		attacker = GetComponent<Attacker>();
	}
		
	void Update () {
	
	}
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		
		GameObject obj = collider.gameObject;
		if(!obj.GetComponent<Defender>())
		{
			//Leave the method if not colliding with defender
			return;
		}
		if(obj.GetComponent<Stone>())
		{
			anim.SetTrigger("jumpTrigger");			
		}
		else
		{
			anim.SetBool ("isAttacking", true);
			attacker.Attack(obj);
		}
	}
}
