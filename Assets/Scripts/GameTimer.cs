﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour {

	public float levelSeconds = 5f;
	private float timeOfFirstMove = 0f;
	private Slider slider;
	private AudioSource audioSource;
	private bool isEndOfLevel;
	private LevelManager levelManager;
	private GameObject winLabel;		
	private bool firstMoveMade = false;
	private float gameTimeElapsed = 0f;
	
	void Start () 
	{
		slider = GetComponent<Slider>();
		audioSource = GetComponent <AudioSource>();		
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		
		FindYouWin ();
		winLabel.SetActive (false);
	}

	public bool GetFirstMoveMade()
	{
		return firstMoveMade;
	}
	public void SetFirstMoveMade(bool setTo)
	{
		firstMoveMade = setTo;
	}
	void FindYouWin ()
	{
		winLabel = GameObject.Find ("You Win");
		if (!winLabel) {
			Debug.LogWarning ("Please create you win object");
		}
	}	
	
	void Update () 
	{
		if(firstMoveMade == true && timeOfFirstMove == 0f)
		{
			timeOfFirstMove = Time.timeSinceLevelLoad;					
		}		
		if(firstMoveMade == true)
		{		
			gameTimeElapsed = (Time.timeSinceLevelLoad-timeOfFirstMove);
			slider.value = (gameTimeElapsed / levelSeconds);
			
			if(gameTimeElapsed >= levelSeconds && !isEndOfLevel)
			{							
				HandleWinCondition ();
			}
		}
	}

	void HandleWinCondition ()
	{
		audioSource.Play ();
		winLabel.SetActive (true);
		Invoke ("LoadNextLevel", audioSource.clip.length);
		isEndOfLevel = true;
	}
	
	void LoadNextLevel()
	{
		levelManager.LoadNextLevel();
	}
}
