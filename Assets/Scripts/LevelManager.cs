﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {

	public float autoLoadNextLevelAfter;
	public List<string> availableDefenders;

	void Start () {
		if (autoLoadNextLevelAfter <= 0) {
			Debug.Log ("Level auto load disabled, use a positive number in seconds");
		} else {
			Invoke ("LoadNextLevel", autoLoadNextLevelAfter);
		}
	}

	public void LoadLevel(string name){
		Debug.Log ("New Level load: " + name);
		Application.LoadLevel (name);
	}

	public void QuitRequest(){
		Debug.Log ("Quit requested");
		Application.Quit ();
	}
	
	public void LoadNextLevel() {
		Debug.Log ("LevelManager.LoadNextLevel()");
		Application.LoadLevel(Application.loadedLevel + 1);
	}
	public List<string> GetAvailableDefenders()
	{
		return availableDefenders;
	}
	public void MakeDefenderAvailable(string defenderName)
	{
		availableDefenders.Add(defenderName);
	}
	public void MakeDefenderUnavailable(string defenderName)
	{
		if(availableDefenders.Contains(defenderName))
		{
			availableDefenders.Remove(defenderName);
		}
		else
		{
			Debug.LogWarning("Defender "+defenderName+" could not be removed");
		}
	}
}
