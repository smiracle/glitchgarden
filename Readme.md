### Glitch Garden
 
A Plants. vs Zombies clone created in 2015.

*    Works on mobile devices and PC
*    Complex animations using copyright-free art from a studio's scrapped game
*    Three different levels with progressing difficulty
*    Four placeable defense "towers" and two enemy types all with various behaviors and animations
*    A decent menu system with adjustable audio levels
*    A simple splash screen